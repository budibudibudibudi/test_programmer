using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainPage : MonoBehaviour
{
    [SerializeField] private TMPro.TMP_Text scoreTeks;
    [SerializeField] private Slider timerSlider;

    private void Start()
    {
        timerSlider.maxValue = GameSetting.Instance.GetTimer();
        timerSlider.value = timerSlider.value;
    }
    private void OnEnable()
    {
        Actions.onScoreChange += OnScoreChange;
        Actions.onTimerChange += OnTimerChange;
    }


    private void OnDisable()
    {
        Actions.onScoreChange -= OnScoreChange;
        Actions.onTimerChange -= OnTimerChange;

    }
    private void OnTimerChange(float obj)
    {
        if (obj <= 0)
            GameManager.Instance.SetState(STATE.LOSE);
        timerSlider.value = obj;
    }
    private void OnScoreChange(int amount)
    {
        scoreTeks.text = $"Score: {amount}";
    }
}
