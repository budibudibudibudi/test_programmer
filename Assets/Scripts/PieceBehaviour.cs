using System;
using System.Collections.Generic;
using UnityEngine;

public class PieceBehaviour : MonoBehaviour
{
    [SerializeField] protected PIECE typePiece;
    [SerializeField] protected int pieceScore;
    [SerializeField] protected bool isMain;
    public event Action onDestroyPiece;
    public virtual void TryMove(Vector2 pos)
    {
    }
    private void OnEnable()
    {
        Actions.deletePiece += DeletePiece;
    }
    private void OnDisable()
    {
        Actions.deletePiece -= DeletePiece;

    }

    public void DeletePiece(PIECE piece)
    {
        if(piece == typePiece)
        {
            if(!isMain)
            {
                Destroy(gameObject);
                onDestroyPiece.Invoke();
            }
        }
    }
    public void SetIsMain(bool value)
    {
        isMain = value;
    }
    public void AddPiece()
    {
        Actions.piecePlace.Invoke(typePiece);

    }
    protected virtual void SendPosToCheck(List<Vector2Int> pos,int score)
    {
        print("send post to check");
        Actions.CheckPieceMove.Invoke(pos,score);
    }
}
public enum PIECE
{
    KUDA,
    BENTENG,
    PELUNCUR,
    DRAGON
}