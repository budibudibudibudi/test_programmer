using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragon : PieceBehaviour
{
    public override void TryMove(Vector2 pos)
    {
        var tempPos = new Vector2Int((int)pos.x, (int)pos.y);
        //keatas
        var targetPos = new List<Vector2Int>();
        targetPos.Add(tempPos + new Vector2Int(1, 0));
        targetPos.Add(tempPos + new Vector2Int(-1, 0));
        targetPos.Add(tempPos + new Vector2Int(0, 1));
        targetPos.Add(tempPos + new Vector2Int(0, -1));
        targetPos.Add(tempPos + new Vector2Int(1, -1));
        targetPos.Add(tempPos + new Vector2Int(1, 1));
        targetPos.Add(tempPos + new Vector2Int(-1, -1));
        targetPos.Add(tempPos + new Vector2Int(-1, 1));


        SendPosToCheck(targetPos, pieceScore);
    }
}
