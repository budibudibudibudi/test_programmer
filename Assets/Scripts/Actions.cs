using System;
using System.Collections.Generic;
using UnityEngine;

public class Actions
{
    public static Action<List<Vector2Int>, int> CheckPieceMove;
    public static Action<string> TileHighlight;
    public static Action<int> onScoreChange;
    public static Action<STATE> onStateChange;
    public static Action<PIECE> piecePlace;
    public static Action<PIECE> deletePiece;
    public static Action<float> onTimerChange;
}