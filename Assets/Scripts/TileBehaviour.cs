using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

public class TileBehaviour : MonoBehaviour
{
    [SerializeField] private SpriteRenderer tileImage;
    private PieceBehaviour _occupingPiece;
    private Vector2 _tilePos;
    [SerializeField] private Button tileClick;

    private PieceBehaviour temp;

    AudioSource audio;
    [SerializeField] private AudioClip sfx;
    private void Start()
    {
        audio = gameObject.AddComponent<AudioSource>();
        audio.playOnAwake = false;
    }
    private void OnMouseEnter()
    {
        switch (GameManager.Instance.GetState())
        {
            case STATE.PLAY:
                tileImage.color = Color.green;
                break;
            default:
                break;
        }
    }
    private void OnMouseExit()
    {
        switch (GameManager.Instance.GetState())
        {
            case STATE.PLAY:
                tileImage.color = Color.white;
                break;
            default:
                break;
        }
    }
    private void OnMouseDown()
    {
        switch (GameManager.Instance.GetState())
        {
            case STATE.PLAY:
                if(GameSetting.Instance.GetTimer()>0)
                {
                    GameSetting.Instance.SetTimer(10);
                    SetPiece(GameSetting.Instance.GetMainBidak());
                }
                break;
            default:
                break;
        }
    }
    public void SetTile(Vector2 pos)
    {
        _tilePos = pos;
        transform.localPosition = pos;
    }

    public void SetPiece(PieceBehaviour piece)
    {
        if (_occupingPiece)
            return;
        _occupingPiece = piece;
        temp = Instantiate(piece, transform);
        temp.onDestroyPiece += Temp_onDestroyPiece;
        temp.SetIsMain(false);
        temp.TryMove(_tilePos);
        temp.AddPiece();
        audio.PlayOneShot(sfx);
        GameSetting.Instance.Init();
    }


    private void Temp_onDestroyPiece()
    {
        _occupingPiece = null;
        temp.onDestroyPiece -= Temp_onDestroyPiece;

    }

    public bool CheckOccupingPiece()
    {
        StartCoroutine(ShowMark());
        return _occupingPiece;
    }

    private IEnumerator ShowMark()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        yield return new WaitForSeconds(1);
        switch (GameManager.Instance.GetState())
        {
            case STATE.PLAY:
                transform.GetChild(0).gameObject.SetActive(false);
                break;
            default:
                break;
        }

    }
}