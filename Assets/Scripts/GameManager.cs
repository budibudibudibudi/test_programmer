using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private STATE state;
    public STATE GetState() { return state; }

    public static GameManager Instance { get; private set; }
    private void Awake()
    {
        Instance = this;
    }

    public void SetState(STATE newState)
    {
        state = newState;
        Actions.onStateChange.Invoke(state);
    }
}

public enum STATE
{
    PLAY,
    PAUSE,
    WIN,
    LOSE
}
