using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverPage : MonoBehaviour
{
    [SerializeField] Button retryBTN;
    private void Start()
    {
        retryBTN.onClick.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().name)) ;
    }
}
