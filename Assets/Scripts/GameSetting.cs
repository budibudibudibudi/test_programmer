
using UnityEngine;

public class GameSetting : MonoBehaviour
{
    [SerializeField] PieceBehaviour mainBidak;
    public PieceBehaviour GetMainBidak() { return mainBidak; }
    [SerializeField] PieceBehaviour[] paraBidak;
    [SerializeField] PieceBehaviour[] bidaks;
    [SerializeField] Transform mainSlot;

    [SerializeField] private int Score;

    [SerializeField] private float Timer = 10;
    public float GetTimer() { return Timer; }
    public void SetTimer(float amount) { Timer = amount; }
    #region singleton
    public static GameSetting Instance { get; private set; }
    private void Awake()
    {
        Instance = this;
    }

    #endregion

    private void Start()
    {
        bidaks = new PieceBehaviour[paraBidak.Length];
        foreach (var item in paraBidak)
        {
            Instantiate(item, mainSlot).SetIsMain(true);

        }
        for (int i = 0; i < mainSlot.childCount; i++)
        {
            bidaks[i] = mainSlot.GetChild(i).GetComponent<PieceBehaviour>();
        }
        Init();
    }

    public void Init()
    {
        for (int i = 0; i < mainSlot.childCount; i++)
        {
            mainSlot.GetChild(i).gameObject.SetActive(false);
        }
        int random = Random.Range(0, bidaks.Length);
        mainBidak = bidaks[random];
        bidaks[random].gameObject.SetActive(true);
    }

    public void AddScore(int amount)
    {
        Score += amount;
        Actions.onScoreChange.Invoke(Score);
    }
    private void Update()
    {
        switch (GameManager.Instance.GetState())
        {
            case STATE.PLAY:
                if (Timer > 0)
                {
                    Timer -= 1 * Time.deltaTime;
                    Actions.onTimerChange.Invoke(Timer);
                }
                else
                {
                    Timer = 0;
                    Actions.onTimerChange.Invoke(Timer);
                }
                break;
            case STATE.PAUSE:
                break;
            case STATE.WIN:
                break;
            case STATE.LOSE:
                Timer = 0;
                Actions.onTimerChange.Invoke(Timer);
                break;
            default:
                break;
        }
    }
}
