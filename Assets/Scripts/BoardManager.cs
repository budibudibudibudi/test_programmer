using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    public int boardWidth, boardLenght;
    [SerializeField] private TileBehaviour tilePrefab;
    private TileBehaviour[,] _tileMap;
    [SerializeField] int bentengList;
    [SerializeField] int peluncurList;
    [SerializeField] int dragonList;
    [SerializeField] int kudaList;

    private void Start()
    {
        TileBehaviour tile;
        var tileCounter = 0;
        _tileMap = new TileBehaviour[boardWidth, boardLenght];
        var pos = Vector2.zero;
        for (var i = 0; i < boardWidth; i++)
        {
            tileCounter++;
            for (var j = 0; j < boardLenght; j++)
            {
                pos.x = i;
                pos.y = j;

                tile = Instantiate(tilePrefab, transform);
                tile.name = $"Tile {j},{i}";
                tile.SetTile(pos);
                _tileMap[i, j] = tile;
                tileCounter++;
            }
        }
    }

    private void OnEnable()
    {
        Actions.CheckPieceMove += CheckMovePos;
        Actions.piecePlace += OnPiecePlace;
    }

    private void OnDisable()
    {
        Actions.CheckPieceMove -= CheckMovePos;
        Actions.piecePlace -= OnPiecePlace;
    }
    private void OnPiecePlace(PIECE obj)
    {
        switch (obj)
        {
            case PIECE.KUDA:
                kudaList++;
                print(kudaList);
                if(kudaList>=3)
                {
                    Actions.deletePiece.Invoke(obj);
                    kudaList = 0;
                }
                break;
            case PIECE.BENTENG:
                bentengList++;
                print(bentengList);
                if (bentengList >= 3)
                {
                    Actions.deletePiece.Invoke(obj);
                    bentengList = 0;
                }
                break;
            case PIECE.PELUNCUR:
                peluncurList++;
                print(peluncurList);
                if (peluncurList >= 3)
                {
                    Actions.deletePiece.Invoke(obj);
                    peluncurList = 0;
                }
                break;
            case PIECE.DRAGON:
                dragonList++;
                print(dragonList);
                if (dragonList >= 3)
                {
                    Actions.deletePiece.Invoke(obj);
                    dragonList = 0;
                }
                break;
            default:
                break;
        }
    }


    private void CheckMovePos(List<Vector2Int> positions,int score)
    {
        if (positions == null) return;
        var targetHasPiece = false;
        
        foreach (var pos in positions)
            if (pos.x < boardWidth && pos.y < boardLenght && pos is { x: >= 0, y: >= 0 })
            {
                var tempBool = _tileMap[pos.x, pos.y].CheckOccupingPiece();
                if (!targetHasPiece) targetHasPiece = tempBool;
            }
        if(targetHasPiece)
        {
            GameManager.Instance.SetState(STATE.LOSE);
        }
        else
        {
            GameSetting.Instance.AddScore(score);
        }
        print($"target has piecee? {targetHasPiece}");
    }

}