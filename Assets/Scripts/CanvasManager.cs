using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour
{
    [SerializeField] GameObject gameoverPanel;

    private void OnEnable()
    {
        Actions.onStateChange += OnStateChange;
    }


    private void OnDisable()
    {
        Actions.onStateChange -= OnStateChange;

    }
    private void OnStateChange(STATE newstate)
    {
        switch (newstate)
        {
            case STATE.PAUSE:
                break;
            case STATE.WIN:
                break;
            case STATE.LOSE:
                gameoverPanel.SetActive(true);
                break;
            default:
                break;
        }
    }

}
